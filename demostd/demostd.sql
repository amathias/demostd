CREATE TABLE demostd.food
(
    id bigint(20) PRIMARY KEY NOT NULL,
    name varchar(255),
    price decimal(19,2)
);
INSERT INTO demostd.food (id, name, price) VALUES (1, 'Joelho', 3.00);
INSERT INTO demostd.food (id, name, price) VALUES (2, 'Coxinha', 2.50);
INSERT INTO demostd.food (id, name, price) VALUES (3, 'Enroladinho', 2.50);
INSERT INTO demostd.food (id, name, price) VALUES (4, 'X-Tudo', 6.00);
INSERT INTO demostd.food (id, name, price) VALUES (5, 'X-Bacon', 7.00);
INSERT INTO demostd.food (id, name, price) VALUES (6, 'Prensado', 7.00);
CREATE TABLE demostd.food_order
(
    id bigint(20) PRIMARY KEY NOT NULL,
    amount int(11),
    food_id bigint(20),
    orders_id bigint(20)
);
CREATE INDEX FKq0bn99dwrnw8vlwcvvaj1dcma ON demostd.food_order (food_id);
CREATE INDEX FKawtwpvl3fl17ntvvo31b9nrtw ON demostd.food_order (orders_id);
CREATE TABLE demostd.hibernate_sequence
(
    next_val bigint(20)
);
INSERT INTO demostd.hibernate_sequence (next_val) VALUES (1);
INSERT INTO demostd.hibernate_sequence (next_val) VALUES (1);
INSERT INTO demostd.hibernate_sequence (next_val) VALUES (1);
INSERT INTO demostd.hibernate_sequence (next_val) VALUES (1);
INSERT INTO demostd.hibernate_sequence (next_val) VALUES (1);
CREATE TABLE demostd.orders
(
    id bigint(20) PRIMARY KEY NOT NULL,
    restaurant_id bigint(20),
    user_id bigint(20)
);
CREATE INDEX FKi7hgjxhw21nei3xgpe4nnpenh ON demostd.orders (restaurant_id);
CREATE INDEX FKel9kyl84ego2otj2accfd8mr7 ON demostd.orders (user_id);
CREATE TABLE demostd.orders_food_orders
(
    orders_id bigint(20) NOT NULL,
    food_orders_id bigint(20) NOT NULL
);
CREATE UNIQUE INDEX UK_94ob1at5333h5t380ynngxi6v ON demostd.orders_food_orders (food_orders_id);
CREATE INDEX FKbjq6r4w9c0owudmekcb5h4lxk ON demostd.orders_food_orders (orders_id);
CREATE TABLE demostd.restaurant
(
    id bigint(20) PRIMARY KEY NOT NULL,
    address varchar(255),
    name varchar(255)
);
INSERT INTO demostd.restaurant (id, address, name) VALUES (1, 'Centro Bangu', 'Geléia');
INSERT INTO demostd.restaurant (id, address, name) VALUES (2, 'Praça Bangu', 'Trailler Podrão');
CREATE TABLE demostd.restaurant_foods
(
    restaurant_id bigint(20) NOT NULL,
    foods_id bigint(20) NOT NULL
);
CREATE INDEX FK4dbvvswitmtp26c2n425c9d12 ON demostd.restaurant_foods (restaurant_id);
CREATE INDEX FK546hh7nv85x4dl0aefekixbm6 ON demostd.restaurant_foods (foods_id);
INSERT INTO demostd.restaurant_foods (restaurant_id, foods_id) VALUES (1, 1);
INSERT INTO demostd.restaurant_foods (restaurant_id, foods_id) VALUES (1, 2);
INSERT INTO demostd.restaurant_foods (restaurant_id, foods_id) VALUES (1, 3);
INSERT INTO demostd.restaurant_foods (restaurant_id, foods_id) VALUES (2, 1);
INSERT INTO demostd.restaurant_foods (restaurant_id, foods_id) VALUES (2, 2);
INSERT INTO demostd.restaurant_foods (restaurant_id, foods_id) VALUES (2, 3);
CREATE TABLE demostd.user
(
    id bigint(20) PRIMARY KEY NOT NULL,
    username varchar(255),
    name varchar(255),
    password varchar(255)
);
INSERT INTO demostd.user (id, username, name, password) VALUES (1, 'droidmarkus', 'Markus', '$2a$10$..1wLf2gusxH8HuHyCNcGem0wzMjU1cc9NHkweuAPjvVNEtpQ5UQ.');
INSERT INTO demostd.user (id, username, name, password) VALUES (2, 'mynameiskara', 'Kara', '$2a$10$C//uMkNWZqvMZigK0GFfv.WnaS4OfbBFiRpkbMktyKRAqAr0JIjrG');
INSERT INTO demostd.user (id, username, name, password) VALUES (3, 'connor800', 'Connor', '$2a$10$DypZLWjRCD/qykwSoM/awOg1gQk5TUn5Y17ddwt8wH9kTFGVmANkG');

ALTER TABLE demostd.food_order
ADD CONSTRAINT food_order_food_id_fk
FOREIGN KEY (food_id) REFERENCES demostd.food (id);
ALTER TABLE demostd.food_order
ADD CONSTRAINT food_order_orders_id_fk
FOREIGN KEY (orders_id) REFERENCES demostd.orders (id);


ALTER TABLE demostd.orders
  ADD CONSTRAINT orders_restaurant_id_fk
FOREIGN KEY (restaurant_id) REFERENCES demostd.restaurant (id);
ALTER TABLE demostd.orders
  ADD CONSTRAINT orders_user_id_fk
FOREIGN KEY (user_id) REFERENCES demostd.user (id);


ALTER TABLE demostd.orders_food_orders
  ADD CONSTRAINT orders_food_orders_orders_id_fk
FOREIGN KEY (orders_id) REFERENCES demostd.orders (id);
ALTER TABLE demostd.orders_food_orders
  ADD CONSTRAINT orders_food_orders_food_order_id_fk
FOREIGN KEY (food_orders_id) REFERENCES demostd.food_order (id);


ALTER TABLE demostd.restaurant_foods
  ADD CONSTRAINT restaurant_foods_restaurant_id_fk
FOREIGN KEY (restaurant_id) REFERENCES demostd.restaurant (id);
ALTER TABLE demostd.restaurant_foods
  ADD CONSTRAINT restaurant_foods_food_id_fk
FOREIGN KEY (foods_id) REFERENCES demostd.food (id);