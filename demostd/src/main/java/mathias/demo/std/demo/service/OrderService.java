package mathias.demo.std.demo.service;

import mathias.demo.std.demo.model.Orders;

public interface OrderService {

    Orders placeOrder(Orders order );
}
