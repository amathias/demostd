package mathias.demo.std.demo.service.impl;

import mathias.demo.std.demo.model.User;
import mathias.demo.std.demo.repository.UserRepository;
import mathias.demo.std.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@Qualifier("UserService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User createUser(User user) {

        // TODO: Validate User

        user.setPassword( passwordEncoder.encode( user.getPassword() ) );

        return this.userRepository.save( user );
    }

    @Override
    public User save(User user) {
        // TODO: Validate User

        user.setPassword( passwordEncoder.encode( user.getPassword() ) );

        return this.userRepository.save( user );
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return this.userRepository.findByUsername( username );
    }

    @Override
    public Optional<User> findById(Long id) {
        return this.userRepository.findById( id );
    }

    @Override
    public Iterable<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final Optional<User> user = this.userRepository.findByUsername( username );

        if ( user.isPresent() ) {

            return org.springframework.security.core.userdetails.User.withUsername( user.get().getUsername() )
                    .password( user.get().getPassword() )
                    .roles("USER")
                    .build();

        }

        throw new UsernameNotFoundException("Usuário não encontrado");
    }
}
