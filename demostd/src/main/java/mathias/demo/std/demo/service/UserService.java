package mathias.demo.std.demo.service;

import mathias.demo.std.demo.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {

    User save( User user );
    User createUser(User user );
    Optional<User> findByUsername( String username );
    Optional<User> findById( Long id );
    Iterable<User> findAll();
}
