package mathias.demo.std.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@JsonIgnoreProperties( {"hibernateLazyInitializer", "handler"} )
public class FoodOrder implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private Integer amount;
    private BigDecimal price;

    @ManyToOne
    private Food food;

    @JsonBackReference
    @ManyToOne
    private Orders orders;

    public FoodOrder() {
    }

    public FoodOrder(int amount, Food food) {
        this.amount = amount;
        this.food = food;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        if ( getAmount() == null || getFood() == null ) {
            return null;
        }
        return getFood().getPrice().multiply( new BigDecimal( getAmount() ));
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoodOrder foodOrder = (FoodOrder) o;
        return Objects.equals(id, foodOrder.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
