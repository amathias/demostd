package mathias.demo.std.demo.repository;

import mathias.demo.std.demo.model.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface OrdersRepository extends CrudRepository<Orders, Long> {

    Collection<Orders> findByUserId( Long userId );
    Optional<Orders> findByIdAndUserId(Long id, Long userId );
}
