package mathias.demo.std.demo.service.impl;

import mathias.demo.std.demo.model.Orders;
import mathias.demo.std.demo.repository.FoodOrderRepository;
import mathias.demo.std.demo.repository.OrdersRepository;
import mathias.demo.std.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private FoodOrderRepository foodOrderRepository;

    public Orders placeOrder(Orders order ) {

        ordersRepository.save( order );

        order.getFoodOrders().forEach( _foodOrder -> {
            _foodOrder.setOrders( order );
        });

        foodOrderRepository.saveAll( order.getFoodOrders() );

        return order;
    }
}
