package mathias.demo.std.demo.service;

import mathias.demo.std.demo.model.Restaurant;

import java.util.Optional;

public interface RestaurantService {

    Iterable<Restaurant> findAll();
    Optional<Restaurant> findById( Long id );
    Restaurant create( Restaurant restaurant );
}
