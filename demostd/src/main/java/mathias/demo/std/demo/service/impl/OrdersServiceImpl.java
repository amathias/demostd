package mathias.demo.std.demo.service.impl;

import mathias.demo.std.demo.model.Orders;
import mathias.demo.std.demo.repository.OrdersRepository;
import mathias.demo.std.demo.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersRepository ordersRepository;

    public Optional<Orders> findByIdAndUserId(Long id, Long userId) {
        return this.ordersRepository.findByIdAndUserId( id, userId );
    }
}
