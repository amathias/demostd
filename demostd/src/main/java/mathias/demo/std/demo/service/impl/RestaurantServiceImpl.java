package mathias.demo.std.demo.service.impl;

import mathias.demo.std.demo.model.Restaurant;
import mathias.demo.std.demo.repository.RestaurantRepository;
import mathias.demo.std.demo.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Override
    public Iterable<Restaurant> findAll() {
        return this.restaurantRepository.findAll();
    }

    @Override
    public Optional<Restaurant> findById(Long id) {
        return this.restaurantRepository.findById( id );
    }

    @Override
    public Restaurant create(Restaurant restaurant) {
        return this.restaurantRepository.save( restaurant );
    }
}
