package mathias.demo.std.demo.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties( {"hibernateLazyInitializer", "handler"} )
public class Orders implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal price;
    private LocalDateTime dateCreated;

    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @ManyToOne( fetch = FetchType.LAZY )
    private User user;

    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @ManyToOne
    private Restaurant restaurant;

    @OneToMany( mappedBy = "orders", cascade = CascadeType.ALL )
    private List<FoodOrder> foodOrders;

    public Orders() {
    }

    public Orders(User user, Restaurant restaurant, List<FoodOrder> foodOrders) {
        this.user = user;
        this.restaurant = restaurant;
        this.foodOrders = foodOrders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {

        if ( getFoodOrders() == null ) {
            return null;
        }

        final Double price = getFoodOrders().stream()
                .mapToDouble( _foodOrder -> _foodOrder.getPrice().doubleValue() )
                .sum();

        return new BigDecimal( price );
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public List<FoodOrder> getFoodOrders() {
        return foodOrders;
    }

    public void setFoodOrders(List<FoodOrder> foodOrders) {
        this.foodOrders = foodOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orders orders = (Orders) o;
        return Objects.equals(id, orders.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
