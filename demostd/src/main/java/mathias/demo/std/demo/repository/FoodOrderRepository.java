package mathias.demo.std.demo.repository;

import mathias.demo.std.demo.model.FoodOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodOrderRepository extends CrudRepository<FoodOrder, Long> {
}
