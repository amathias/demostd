package mathias.demo.std.demo.service;

import mathias.demo.std.demo.model.Orders;

import java.util.Optional;

public interface OrdersService {

    Optional<Orders> findByIdAndUserId(Long id, Long userId);
}
