package mathias.xprssdlvry.sbrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("mathias.demo.std.demo.model")
@EnableJpaRepositories("mathias.demo.std.demo.repository")
@ComponentScan({"mathias.xprssdlvry.sbrest", "mathias.demo.std.demo"})
public class SbRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbRestApplication.class, args);
    }
}
