package mathias.xprssdlvry.sbrest.controller;

import mathias.demo.std.demo.model.User;
import mathias.demo.std.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user")
    ResponseEntity<User> loadUser( Principal principal ) {

        final Optional<User> user = this.userService.findByUsername( principal.getName() );

        if ( user.isPresent() ) {
            return ResponseEntity.ok( user.get() );
        }

        return ResponseEntity.notFound().build();
    }

    @PutMapping("/user")
    ResponseEntity<User> updateUser( Principal principal, @RequestBody User user ) {

        final Optional<User> loadedUser = this.userService.findByUsername(  principal.getName() );

        user.setId( loadedUser.orElseGet( User::new ).getId() );
        this.userService.save( user );

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path( "/user/{id}" )
                .buildAndExpand( user.getId() ).toUri();

        return ResponseEntity.created( location ).build();
    }

    @PostMapping("/register")
    ResponseEntity<User> createUser( @RequestBody User user ) {

        user = this.userService.createUser( user );

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path( "/user/{id}" )
                .buildAndExpand( user.getId() ).toUri();

        return ResponseEntity.created( location ).build();
    }

}
