package mathias.xprssdlvry.sbrest.controller;

import mathias.demo.std.demo.model.Restaurant;
import mathias.demo.std.demo.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/restaurant")
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @GetMapping
    Iterable<Restaurant> readRestaurants() {
        return restaurantService.findAll();
    }

    @GetMapping("/{restaurantId}")
    ResponseEntity<Restaurant> loadRestaurant ( @PathVariable Long restaurantId ) {

        final Optional<Restaurant> restaurant = restaurantService.findById( restaurantId );

        if ( restaurant.isPresent() ) {
            return ResponseEntity.ok( restaurant.get() );
        }

        return ResponseEntity.notFound().build();

    }

    @PostMapping
    ResponseEntity<Restaurant> addRestaurant( Principal principal, @RequestBody Restaurant restaurant ) {

        restaurant = restaurantService.create( restaurant );

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path( "/user/{id}" )
                .buildAndExpand( restaurant.getId() ).toUri();

        return ResponseEntity.created( location ).build();
    }


}
