package mathias.xprssdlvry.sbrest.controller;

import mathias.demo.std.demo.model.Orders;
import mathias.demo.std.demo.model.User;
import mathias.demo.std.demo.service.OrderService;
import mathias.demo.std.demo.service.OrdersService;
import mathias.demo.std.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.util.Optional;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private UserService userService;

    @PostMapping("/order")
    ResponseEntity<Orders> placeOrder( Principal principal, @RequestBody Orders order ) {

        final Optional<User> loadedUser = this.userService.findByUsername( principal.getName() );

        order.setUser( loadedUser.orElseThrow( () -> new UsernameNotFoundException("Usuário não encontrado")) );

        order = orderService.placeOrder( order );

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path( "/order/{id}" )
                .buildAndExpand( order.getId() ).toUri();

        return ResponseEntity.created( location ).build();
    }

    @GetMapping("/order/{orderId}")
    ResponseEntity<Orders> loadOrder( Principal principal, @PathVariable Long orderId ) {

        final Optional<User> loadedUser = this.userService.findByUsername( principal.getName() );

        final Optional<Orders> order = ordersService.findByIdAndUserId( orderId, loadedUser.orElseGet( User::new ).getId() );

        if ( order.isPresent() ) {
            return ResponseEntity.ok( order.get() );
        }

        return ResponseEntity.noContent().build();
    }

}
